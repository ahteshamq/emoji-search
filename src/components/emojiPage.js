import React from "react";
import Header from './header';
import ShowEmojis from './showEmoji'
import '../style/emojiPage.css'

class EmojiPage extends React.Component {
    state = {
        value: ''
    };

    getSearchText = (event) => {
        this.setState({ 
            value: event.target.value 
        });
    }

    render() {
        return (
            <div className="emojiBody">
                <Header />
                <input className="searchBar" type='text' placeholder="Search emojis.." onChange={this.getSearchText} />
                <ShowEmojis emojiSearched={this.state.value} />
            </div>
        )
    }
}
export default EmojiPage;