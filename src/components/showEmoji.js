import React, { Component } from "react";
import '../style/showEmoji.css'
import emojis from '../emojis.json';

class ShowEmojis extends Component {
    
    render() {
        
        let result = "";
        let searchText = this.props.emojiSearched.toLowerCase().trim();
        
        if (searchText !== "") {
            result = emojis.filter(emoji => {
                let titleArr = emoji.title.toLowerCase();
                return titleArr.includes(searchText)
            })

            if (result.length === 0) {
                result = emojis.filter(emoji => {
                    let keyword = emoji.keywords.toLowerCase()
                    return keyword.includes(searchText)
                })
            }

            if(result.length === 0){
                result = emojis.filter(emoji =>{
                    return emoji.symbol === searchText
                })
            }
        } else {
            result = emojis
        }
        result = result.slice(0, 20)
        return (
            <div className="container">
                {
                    result.length > 0
                        ?
                        result.map((emoji,index) => {
                            console.log(emoji.title)
                            return <div 
                            className="emoji-area" 
                            onClick={() => { navigator.clipboard.writeText(emoji.symbol); }}
                            key={index}>
                                <div className="emoji">
                                    {emoji.symbol}
                                    <span>{emoji.title}</span>
                                </div>
                                <span className="clickText">Click to copy symbol</span>
                            </div>
                        })
                        : <span className="noEmoji">No emoji found, try different keyword.</span>
                }
            </div>
        )
    }
}

export default ShowEmojis;