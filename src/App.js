import EmojiPage from './components/emojiPage';
import './App.css';

function App() {
  return (
    <div className='App'>
      <EmojiPage/>
    </div>
  );
}

export default App;
